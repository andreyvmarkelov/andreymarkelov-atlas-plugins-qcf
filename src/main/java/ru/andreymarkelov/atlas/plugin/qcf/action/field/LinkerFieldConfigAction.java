package ru.andreymarkelov.atlas.plugin.qcf.action.field;

import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import ru.andreymarkelov.atlas.plugin.qcf.manager.LinkerFieldConfigManager;

import java.util.LinkedHashMap;
import java.util.Map;

public class LinkerFieldConfigAction extends BaseQueryFieldConfigAction {
    private final LinkerFieldConfigManager linkerFieldConfigManager;

    private String epicCalculateMode;

    public LinkerFieldConfigAction(
            ManagedConfigurationItemService managedConfigurationItemService,
            AutoCompleteJsonGenerator autoCompleteJsonGenerator,
            LinkerFieldConfigManager linkerFieldConfigManager) {
        super(managedConfigurationItemService, autoCompleteJsonGenerator);
        this.linkerFieldConfigManager = linkerFieldConfigManager;
    }

    @Override
    public String doDefault() throws Exception {
        //epicCalculateMode = progressTrackerFieldConfigManager.getEpicCalculateMode(getFieldConfig()).toString();
        return INPUT;
    }

    @Override
    @com.atlassian.jira.security.xsrf.RequiresXsrfCheck
    protected String doExecute() throws Exception {
        if (!hasGlobalPermission(GlobalPermissionKey.ADMINISTER)) {
            return "securitybreach";
        }

        //progressTrackerFieldConfigManager.setEpicCalculateMode(getFieldConfig(), EpicCalculateMode.valueOf(epicCalculateMode));
        return getRedirect("/secure/admin/ConfigureCustomField!default.jspa?customFieldId=" + getFieldConfig().getCustomField().getIdAsLong().toString());
    }

    public Map<String, String> getAllOptions() {
        Map<String, String> options = new LinkedHashMap<>();
        options.put("key", "queryfields.opt.key");
        options.put("status", "queryfields.opt.status");
        options.put("assignee", "queryfields.opt.assignee");
        options.put("due", "queryfields.opt.due");
        options.put("priority", "queryfields.opt.priority");
        return options;
    }

    public String getEpicCalculateMode() {
        return epicCalculateMode;
    }

    public void setEpicCalculateMode(String epicCalculateMode) {
        this.epicCalculateMode = epicCalculateMode;
    }
}
